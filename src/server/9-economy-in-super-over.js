function economySuper(deliveries) {
    let superOverBalls = deliveries.filter(balls => balls.is_super_over == true)
    let runCount = superOverBalls.reduce((storedRunObject, ball) => {
        if (storedRunObject.hasOwnProperty(ball.bowler)) {
            storedRunObject[ball.bowler] += parseInt(ball.total_runs);
        } else {
            storedRunObject[ball.bowler] = parseInt(ball.total_runs);
        }
        return storedRunObject;
    }, {})
    let ballCount = superOverBalls.reduce((storedBallObject, ball) => {
        if (storedBallObject.hasOwnProperty(ball.bowler)) {
            storedBallObject[ball.bowler] += 1;
        } else {
            storedBallObject[ball.bowler] = 1;
        }
        return storedBallObject;
    }, {});
    let runArray = Object.entries(runCount);
    let ballArray = Object.entries(ballCount);
    runArray.forEach(element => {
        ballArray.forEach(ball => {
            if (element[0] === ball[0]) {
                element[1] = parseFloat((element[1] / ball[1]) * 6);
            }
        })
    });
    let sortedArray = runArray.sort(
        (p1, p2) =>
            (p1[1] < p2[1]) ? 1 : (p1[1] > p2[1]) ? -1 : 0);

    let answer = sortedArray.pop();
    return answer;
}

module.exports = economySuper;



