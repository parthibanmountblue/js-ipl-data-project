function matchesWonPerYear(matches) {
  let WonPerYear = matches.reduce((storedValue, match) => {
    let year = match.season;
    let team = match.winner;
    if (storedValue.hasOwnProperty(year)) {
      if (storedValue[year].hasOwnProperty(team)) {
        storedValue[year][team] += 1;
      } else {
        storedValue[year][team] = 1;
      }
    } else {
      storedValue[year] = {};
      storedValue[year][team] = 1;
    }
    return storedValue;
  }, {});
  return WonPerYear;
}

module.exports = matchesWonPerYear;
