function playerDismissal(deliveries) {
  let dismissalBatsmanBowler = deliveries
    .filter((delivery) => delivery.player_dismissed === delivery.batsman)
    .map((delivery) => {
      return [delivery.batsman, delivery.bowler];
    });
  let sameDismissal = dismissalBatsmanBowler.reduce(function (
    objBatsmanBowler,
    batsmanBowler
  ) {
    if (objBatsmanBowler.hasOwnProperty(batsmanBowler)) {
      objBatsmanBowler[batsmanBowler] += 1;
    } else {
      objBatsmanBowler[batsmanBowler] = 1;
    }
    return objBatsmanBowler;
  },
    {});
  const sortDismissal = Object.fromEntries(
    Object.entries(sameDismissal)
      .sort(([, a], [, b]) => b - a)
      .slice(0, 1)
  );
  return sortDismissal;
}

module.exports = playerDismissal;
