function strikeRate(matches, delivery) {
    let matchesSeason = matches.reduce((storedValue, match) => {
        storedValue[match.id] = match.season;
        return storedValue;
    }, {});
    let playersDetails = delivery.reduce((accumulater, delivery) => {
        let season = matchesSeason[delivery.match_id];
        if (accumulater.hasOwnProperty(delivery.batsman)) {
            if (accumulater[delivery.batsman].hasOwnProperty(season)) {
                accumulater[delivery.batsman][season].totalRuns += parseInt(delivery.total_runs);
                accumulater[delivery.batsman][season].totalBallsFaced += 1;
            } else {
                accumulater[delivery.batsman][season] = {};
                accumulater[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
                accumulater[delivery.batsman][season].totalBallsFaced = 1;
            }
        }else {
            accumulater[delivery.batsman] = {};
            accumulater[delivery.batsman][season] = {};
            accumulater[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
            accumulater[delivery.batsman][season].totalBallsFaced = 1;
        }
        return accumulater;
    }, {});
    const result = {};
    Object.keys(playersDetails).map((batsman) => {
        let strike = {};
        Object.keys(playersDetails[batsman]).map(year => {
            let answer = (playersDetails[batsman][year].totalRuns / playersDetails[batsman][year].totalBallsFaced) * 100;
            strike[year] = answer.toFixed(2);
        })
        result[batsman] = strike;
    });
    return result;
}

module.exports = strikeRate;



