function extra(matches, deliveries, givenYear) {
  let id = matches
    .filter((match) => match.season == givenYear)
    .map((match) => match.id);
  let extraRuns = deliveries.reduce((objTeam, delivery) => {
    let team = delivery.bowling_team;
    let extras = delivery.extra_runs;
    if (id.includes(delivery.match_id) && extras !== "0") {
      if (objTeam.hasOwnProperty(team)) {
        objTeam[team] += parseInt(extras);
      } else {
        objTeam[team] = parseInt(extras);
      }
    }
    return objTeam;
  }, {});
  return extraRuns;
}

module.exports = extra;
