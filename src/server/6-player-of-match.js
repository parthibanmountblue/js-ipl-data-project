function highestManOfMatch(matches) {
  let manOfMatch = {};
  let years = matches.map((match) => match.season);
  years = years.filter((item, index, arr) => arr.indexOf(item) === index);
  years.forEach(year => {
      let myObj = {};
      matches.forEach(match => {
          if (match.season === year) {
              if (myObj.hasOwnProperty(match.player_of_match)) {
                  myObj[match.player_of_match] += 1;
              } else {
                  myObj[match.player_of_match] = 1;
              }
          }
      })
      const sortable = Object.fromEntries(
          Object.entries(myObj).sort(([, a], [, b]) => b - a))
      manOfMatch[year] = Object.keys(sortable)[0]
  })
  return manOfMatch;
}

module.exports = highestManOfMatch;




