function matchesPerYear(matches) {
    let output = matches.reduce((storedObject, match) => {
        let season = match.season;
        if (storedObject.hasOwnProperty(season)) {
            storedObject[season] += 1;
        } else {
            storedObject[season] = 1;
        }
        return storedObject;
    }, {});
    return output;
}

module.exports = matchesPerYear;