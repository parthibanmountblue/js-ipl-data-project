function economicBowler(matches, delivery,givenYear) {
    let matchesInYear = matches.filter(matches => matches.season == givenYear);
    let matchId = matchesInYear.map(match => match.id);
    let deliveryInYear = delivery.filter(delivery => matchId.includes(delivery.match_id));
    let runCount = deliveryInYear.reduce((runCountObject, balls) => {
        if (runCountObject.hasOwnProperty(balls.bowler)) {
            runCountObject[balls.bowler] += parseInt(balls.total_runs);
        } else {
            runCountObject[balls.bowler] = parseInt(balls.total_runs);
        }
        return runCountObject;

    }, {});
    let ballCount = deliveryInYear.reduce((ballCountObject, balls) => {
        if (ballCountObject.hasOwnProperty(balls.bowler)) {
            if ((balls.wide_runs == 0) && (balls.noball_runs == 0)) {
                ballCountObject[balls.bowler] += 1;
            }
        } else {
            ballCountObject[balls.bowler] = 1;
        }
        return ballCountObject;
    }, {});
    let runArray = Object.entries(runCount);
    let ballArray = Object.entries(ballCount);
    runArray.forEach(run => {
        run[1] = parseFloat((((parseInt(run[1]) / parseInt(ballCount[run[0]])) * 6))).toFixed(2);
    })
    let result = runArray.sort(
        (p1, p2) =>
            (parseFloat(p1[1]) > parseFloat(p2[1]) ? 1 : parseFloat(p1[1]) < parseFloat(p2[1]) ? -1 : 0)
    );
    result = result.slice(0, 10);
    return result;
}

module.exports = economicBowler;
