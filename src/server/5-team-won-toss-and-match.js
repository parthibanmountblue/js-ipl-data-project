function tossMatch(matches) {
    let filteredMatch = matches.filter(match => match.winner === match.toss_winner);
    let result = filteredMatch.reduce((storedValue, match) => {
        if (storedValue.hasOwnProperty(match.winner)) {
            storedValue[match.winner] += 1;
        } else {
            storedValue[match.winner] = 1;
        }
        return storedValue;
    }, {})
    return result;
}

module.exports = tossMatch;