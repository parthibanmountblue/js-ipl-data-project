
//to get path module
const path = require("path");

// path showing location of data files
const csvMatchesPath = path.join(__dirname, "../data/matches.csv");
const csvDeliveriesPath = path.join(__dirname, "../data/deliveries.csv");

// module for converting csv to json
const csv = require("csvtojson");
const fs = require("fs");

// 1)matches in year

const matchesPerYear = require(path.join(__dirname,'./1-matches-per-year.js'))
csv()
.fromFile(csvMatchesPath)
.then( (matches) => {
    let result = matchesPerYear(matches)

    fs.writeFileSync(path.join(__dirname,'../public/output/matchesperyear.json'),JSON.stringify(result))
})

// 2)matches won per team per year

const matchesWonPerTeamPerYear = require(path.join(__dirname,'./2-matches-won-per-team-per-year.js'))
csv()
.fromFile(csvMatchesPath)
.then( (matches) => {
    let result = matchesWonPerTeamPerYear(matches)

    fs.writeFileSync(path.join(__dirname,'../public/output/matcheswonperteamperyear.json'),JSON.stringify(result))
})

//3) extra runs per team

const extraRunsPerTeam = require(path.join(__dirname,'./3-extra-runs-per-team-in-2016.js'))
csv()
.fromFile(csvMatchesPath)
.then( (matches) => {
    csv()
    .fromFile(csvDeliveriesPath)
    .then( (deliveries) => {
    let givenYear = 2016;

    let result = extraRunsPerTeam(matches, deliveries,givenYear)

    fs.writeFileSync(path.join(__dirname,'../public/output/extrarunsperteam.json'),JSON.stringify(result))
    })
})

//4)economic bowlers in 2016

const economicalBowlers = require(path.join(__dirname,'./4-economic-bowlers-of-2015.js'))
csv()
.fromFile(csvMatchesPath)
.then( (matches) => {
    csv()
    .fromFile(csvDeliveriesPath)
    .then( (deliveries) => {

    let givenYear = 2015;

    let result = economicalBowlers(matches, deliveries,givenYear)

    fs.writeFileSync(path.join(__dirname,'../public/output/economicbowlersof2015.json'),JSON.stringify(result))
    })
})

//5)team won toss and match

const tossAndMatchWin = require(path.join(__dirname,'./5-team-won-toss-and-match.js'))
csv()
.fromFile(csvMatchesPath)
.then( (matches) => {
    let result = tossAndMatchWin(matches)

    fs.writeFileSync(path.join(__dirname,'../public/output/teamwontossandmatch.json'),JSON.stringify(result))
})

//6)player of match

const playerOfTheMatch = require(path.join(__dirname,'./6-player-of-match.js'))
csv()
.fromFile(csvMatchesPath)
.then( (matches) => {

    let result = playerOfTheMatch(matches)

    fs.writeFileSync(path.join(__dirname,'../public/output/highestManOfMatch.json'),JSON.stringify(result))
})

//7)strike rate of batsman

const strikeRate = require(path.join(__dirname,'./7-strike-rate-of-batsman.js'))
csv()
.fromFile(csvMatchesPath)
.then( (matches) => {
    csv()
    .fromFile(csvDeliveriesPath)
    .then( (deliveries) => {

    let result = strikeRate(matches, deliveries)

    fs.writeFileSync(path.join(__dirname,'../public/output/strikerateofbatsman.json'),JSON.stringify(result))
    })
})

//8) most  player dismissal

const playerDismissed = require(path.join(__dirname,'./8-player-dismissed-by-other.js'))
csv()
  .fromFile(csvDeliveriesPath)
  .then((deliveries) => {
    let result = playerDismissed(deliveries);
    fs.writeFileSync( path.join(__dirname,'../public/output/playerdismissedbyother.json'),JSON.stringify(result))
  })


//9) economy in super over

const bestEconomyBowler = require(path.join(__dirname,'./9-economy-in-super-over.js'))
csv()
  .fromFile(csvDeliveriesPath)
  .then((deliveries) => {

    let result = bestEconomyBowler(deliveries)

    fs.writeFileSync( path.join(__dirname,'../public/output/economyinsuperover.json'),JSON.stringify(result))
  });


